import * as NodeFs from "fs"
import * as NodePath from "path"

import { ObjectTreeParser } from "ts-fusion-parser"

const fusionFilePath = "data/test.fusion"
const fusionFileContent = NodeFs.readFileSync(fusionFilePath).toString()

const parsedFusionFile = ObjectTreeParser.parse(fusionFileContent)

console.log(parsedFusionFile)