# ts-fusion-printer

## Contribution

1. Clone repository
2. `yarn install`
3. `yarn run watch`

### Requirements

#### Optional

This repository uses `watchexec` to watch files and rebuild the project using `esbuild`

``` bash
brew install watchexec
```
